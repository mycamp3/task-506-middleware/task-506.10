const getAllCourseMiddleware = (req, res, next) => {
    console.log("GET all course middleware");
    
    next();
}

const createCourseMiddleware = (req, res, next) => {
    console.log("POST course middleware");
    
    next();
}
const getourseByIdMiddleware = (req, res, next) => {
    console.log("GET course by id middleware");
    
    next();
}
const updateCourseMiddleware = (req, res, next) => {
    console.log("UPDATE course middleware");
    
    next();
}
const deleteCourseMiddleware = (req, res, next) => {
    console.log("DELETE course middleware");
    
    next();
}
module.exports = {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getourseByIdMiddleware,
    updateCourseMiddleware,
    deleteCourseMiddleware
}