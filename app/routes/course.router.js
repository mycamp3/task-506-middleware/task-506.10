const express = require("express");
 
const router = express.Router();

const {
    getAllCourseMiddleware,
    createCourseMiddleware,
    getourseByIdMiddleware,
    deleteCourseMiddleware,
    updateCourseMiddleware
} = require("../middlewares/course.middleware")

router.get("/", getAllCourseMiddleware, (req, res) => {
    res.json( {
        message: "Get all courses"
    })
});

router.post("/", createCourseMiddleware, (req, res) => {
    res.json({
        message: "POST course"
    })
})

router.get("/:courseid", getourseByIdMiddleware, (req, res) => {
    const courseid = req.params.courseid
    res.json( {
        message: "Get course id = " + courseid
    })
});

router.put("/:courseid", updateCourseMiddleware, (req, res) => {
    const courseid = req.params.courseid
    res.json( {
        message: "Update course id = " + courseid
    })
});

router.delete("/:courseid", deleteCourseMiddleware, (req, res) => {
    const courseid = req.params.courseid
    res.json( {
        message: "Delete course id = " + courseid
    })
});
module.exports = router;